* Checkout files from the repos:

    https://gitlab.com/SharpenNG/sharpen
    
    https://gitlab.com/SharpenNG/sharpen_config
    
    https://gitlab.com/SharpenNG/Sharpen.Runtime
    
* Build and install the SharpenNG project
    - CD to the sharpen directory
    - mvn clean package install
    - Copy the ./src/target/sharpencore-0.0.1-SNAPSHOT-jar-with-dependencies.jar
      file to a convenient place like ~/sharpen.jar

* Build the sharpen_config project package
    - CD to the sharpen_config directory
    - mvn clean package
    - Copy the ./sharpen.config/target/MEConfiguration.sharpenconfig-jar-with-dependencies.jar
      file to a convenient place like ~/sharpenconfig.jar

* Build the Sharpen.Runtime project .NET library
    - CD to the Sharpen.Runtime directory
    - Open Sharpen.sln in MonoDevelop or Visual Studio
    - Build through the build menu

* Copy the sharpen JAR file (with dependencies) to a convenient location
    - cp src/target/sharpencore-0.0.1-SNAPSHOT-jar-with-dependencies.jar ~/sharpen.jar

Example full command with unit test conversion:

    java -jar ~/sharpen.jar -configJarFilePath ~/sharpenconfig.jar 
        -configurationClass sharpen.config.MEConfiguration -outputFolder ../csharp 
        -junitConversion -cp ~/.m2/repository/junit/junit/4.11/junit-4.11.jar ./src/
